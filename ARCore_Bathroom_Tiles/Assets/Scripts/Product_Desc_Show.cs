using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Product_Desc_Show : MonoBehaviour
{
    public GameObject[] product_Obj;
    public RotateObject rotator;
    public Transform parent_of_InstObj;
    public Text pro_Name;
    public Text pro_Price;
    public Text pro_Desc;
    string pref_Key;
    GameObject instantiated_Obj;

    public static Product_Desc_Show instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {

    }
    public void Product_Desc_Display(string product_Name)
    {
        Product_Obj_Instantiate(product_Name);
        PlayerPrefs.SetString("Product_Name", product_Name);
        for (int i = 0; i < LoadAssets.instance.loaded_Pro.Length; i++)
        {
            if (LoadAssets.instance.loaded_Pro[i].name == product_Name)
            {

                //pro_Img.sprite = LoadAssets.instance.loaded_Pro_Desc[i].product_Img;
                pro_Name.text = LoadAssets.instance.loaded_Pro[i].product_Name;
                pro_Price.text = LoadAssets.instance.loaded_Pro[i].product_Price;
                pro_Desc.text = LoadAssets.instance.loaded_Pro[i].product_Description;
            }
        }

    }

    public void TakeAScreenShot()
    {
        string ssName = $"AR_Bathroom";
        NativeToolkit.SaveScreenshot(ssName);
        //Debug.Log($"{NativeToolkit.GetAndroidImageSavePat()}");
    }
    public void ResetObject()
    {
        rotator.ResetObject();
    }

    void Product_Obj_Instantiate(string pro_name)
    {
        if (parent_of_InstObj.childCount != 0)
        {
            Destroy(parent_of_InstObj.GetChild(0).gameObject);
        }

        GameObject go = Array.Find(product_Obj, x => x.name == pro_name);
        if (go != null)
        {
            instantiated_Obj = Instantiate(go);
            instantiated_Obj.transform.SetParent(parent_of_InstObj);
            rotator = instantiated_Obj.GetComponent<RotateObject>();
        }
    }

}
