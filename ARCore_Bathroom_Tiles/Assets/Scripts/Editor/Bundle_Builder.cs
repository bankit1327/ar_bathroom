using UnityEngine;
using UnityEditor;

public class Bundle_Builder : Editor
{
    [MenuItem("Assets/AssetBundles")]
    
    static void BuildAllAssetBundle()
    {
        BuildPipeline.BuildAssetBundles(@"C:\Dhara\Dhara_Unity_Projects\AR_Bathroom_Git\ar_bathroom\ARCore_Bathroom_Tiles\Assets\StreamingAssets", BuildAssetBundleOptions.ChunkBasedCompression,BuildTarget.StandaloneWindows64);
    }

}
