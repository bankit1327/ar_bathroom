using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.IO;
using System;
using UnityEditor;

public class LoadAssets : MonoBehaviour
{
    public Text AssetLoadedText;
    AssetBundle scriptable_Bundle;
    public string path;
    public Product[] loaded_Pro;
    // Start is called before the first frame update

    //public Spawned_Obj_Scriptable _Obj;
    public static LoadAssets instance;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //path = Path.Combine(Application.streamingAssetsPath, "scriptable_objects");
        //Load_Obj_From_Bundle();
        //LoadScriptableObjs();
    }

    void LoadScriptableObjs()
    {
        //path = Path.Combine(Application.streamingAssetsPath, "scriptable_objects");
        //path = $"{Application.streamingAssetsPath}/scriptable_objects";
        StartCoroutine(LoadTheAssetBundle((bundle) =>
        {
            loaded_Pro = bundle.LoadAllAssets<Product>();
            
            //Debug.Log($"Array Size of the assetbundle {loaded_Pro.Length}");
            //AssetLoadedText.text = "Assets Loaded Successfully: "+loaded_Pro_Desc.Length.ToString();
            //Debug.Log()
        }));
    }

    private IEnumerator LoadTheAssetBundle(Action<AssetBundle> callback)
    {
        AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(path);
        yield return request;

        AssetBundle bundle = request.assetBundle;
        callback(bundle);
    }

    //PlayerSettings.spl


    void Load_Obj_From_Bundle()
    {
        //path = Path.Combine(Application.streamingAssetsPath, "scriptable_objects");
 

        Debug.Log(scriptable_Bundle == null ? "Failed to load": "Successfully Loaded");
        if(scriptable_Bundle != null)
        {
            loaded_Pro = scriptable_Bundle.LoadAllAssets<Product>();
            //AssetLoadedText.text = "Scriptable Objects Laoded: " +loaded_Pro.Length.ToString();
        }
    }
}
