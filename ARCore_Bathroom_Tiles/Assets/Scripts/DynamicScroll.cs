using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamicScroll : MonoBehaviour
{
    public Sprite[] sprite_Btn;


    public Sprite[] bath_Product_Sprites;
    public string[] bath_Product_Name_Price;

    public Sprite[] tiles_Sprites;
    public string[] tiles_Name_Price;


    public Transform scroll_Content_Transform;
    public GameObject scroll_Btn;

    GameObject spawnObj;
    //string spawnObjName;
    int index;
    public int pro_num;
    //public int st_Index;
    // Start is called before the first frame update
    void Start()
    {
        //Scroll_Item_Display();
    }

    public void Scroll_Item_Display(string clicked_Btn_Name)
    {
        UI_Manager.instance.Add_Current_Screen(UI_Manager.instance.product_category_Screen);
        UI_Manager.instance.product_category_Screen.SetActive(false);
        UI_Manager.instance.product_list_Screen.SetActive(true);
        RemoveChildObj();

        for (int i = 0; i < pro_num; i++)
        {

            spawnObj = Instantiate(scroll_Btn);
            spawnObj.transform.SetParent(scroll_Content_Transform);

            if (index < sprite_Btn.Length)
            {
                spawnObj.GetComponent<Image>().sprite = sprite_Btn[index];
                index++;
            }
            else if (index == sprite_Btn.Length)
            {
                index = 0;
                spawnObj.GetComponent<Image>().sprite = sprite_Btn[index];
                index++;
            }


            if (clicked_Btn_Name == "Bathroom_Products")
            {

                //   Bath_Asseccories_Clicked(i);
                spawnObj.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = bath_Product_Sprites[i];
                spawnObj.GetComponentInChildren<Text>().text = bath_Product_Name_Price[i];
                spawnObj.transform.localScale = new Vector3(1f, 1f, 1f);
                string name = bath_Product_Name_Price[i].Substring(0, bath_Product_Name_Price[i].IndexOf(' '));
                //    Debug.Log(spawnObjName);
                spawnObj.name = name + "_Btn";
                spawnObj.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => Product_Btn_Click(name));

            }
            else if (clicked_Btn_Name == "Tiles")
            {
                //Tiles_Clicked(i);
                spawnObj.transform.GetChild(1).GetChild(0).GetComponent<RectTransform>().localScale = new Vector3(0.8f, 1f, 1f);
                spawnObj.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = tiles_Sprites[i];
                spawnObj.GetComponentInChildren<Text>().text = tiles_Name_Price[i];
                spawnObj.transform.localScale = new Vector3(1f, 1f, 1f);
                string name = tiles_Name_Price[i].Substring(0, tiles_Name_Price[i].IndexOf(' '));
                spawnObj.name = name + "_Btn";
                spawnObj.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => Product_Btn_Click(name));
            }

            //spawnObj.GetComponent<Button>().onClick.AddListener(() => Product_Btn_Click(name));




            //scr_Btn.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = product_Sprite[i];
            //if(st_Index < product_Sprite.Length)
            //{
            //    //scr_Btn.GetComponent<Text>().text = Btn_Text[st_Index].text;
            //    scr_Btn.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = product_Sprite[st_Index];
            //    st_Index++;
            //}
            //else if (st_Index == product_Sprite.Length)
            //{
            //    if(st_Index < product_Sprite.Length)

            //    st_Index = 0;
            //    //scr_Btn.GetComponent<Text>().text = Btn_Text[st_Index].text;
            //    scr_Btn.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = product_Sprite[st_Index];
            //    st_Index++;
            //}
        }
    }

    public void Bath_Asseccories_Clicked(int i)
    {

        //Scroll_Item_Display();

        spawnObj.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = bath_Product_Sprites[i];
        spawnObj.GetComponentInChildren<Text>().text = bath_Product_Name_Price[i];
        string name = bath_Product_Name_Price[i].Substring(0, bath_Product_Name_Price[i].IndexOf(' '));
        spawnObj.name = name + "_Btn";
        spawnObj.GetComponent<Button>().onClick.AddListener(() => Product_Btn_Click(name));

    }

    public void Tiles_Clicked(int i)
    {
        spawnObj.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = tiles_Sprites[i];
        spawnObj.GetComponentInChildren<Text>().text = tiles_Name_Price[i];
        string name = tiles_Name_Price[i].Substring(0, tiles_Name_Price[i].IndexOf(' '));
        spawnObj.name = name + "_Btn";
        spawnObj.GetComponent<Button>().onClick.AddListener(() => Product_Btn_Click(name));
    }
    public void RemoveChildObj()
    {
        for (int i = 0; i < scroll_Content_Transform.childCount; i++)
        {
            Destroy(scroll_Content_Transform.GetChild(i).gameObject);
        }
    }

    public void Product_Btn_Click(string product_name)
    {
        UI_Manager.instance.Add_Current_Screen(UI_Manager.instance.product_list_Screen);
        
        UI_Manager.instance.product_list_Screen.SetActive(false);
        UI_Manager.instance.product_desc_Screen.SetActive(true);
        scroll_Content_Transform.localPosition = new Vector3(0f, 0f, 0f);
        gameObject.SetActive(false);
        Camera.main.fieldOfView = 60;
        Product_Desc_Show.instance.Product_Desc_Display(product_name);
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
