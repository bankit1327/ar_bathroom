 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new product", menuName = "Products")]


public class Product : ScriptableObject
{
    //public Product instance;

    //public GameObject product_Obj;
    public string product_Name;
    public string product_Price;
    public string product_Description;


    
}