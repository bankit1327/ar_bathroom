using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zooming_Obj : MonoBehaviour
{
    Camera mainCamera;
    RaycastHit hitInfo;

    public Text text;
    public static bool zoomObj = false;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GetComponent<Camera>();
        
    }

    // Update is called once per frame
    void Update()
    {

        Ray ray = UI_Manager.instance.canvasCamera.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.red);

        if(Physics.Raycast(ray, out hitInfo))
        { 
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                Scroll_Zoom();
                //Product_Desc_Show.instance.rotator.GetComponent<RotateObject>().enabled = false;
            
            }


            if (Input.touchCount == 2)
            {
                zoomObj = true;
                Touch first_Touch = Input.GetTouch(0);
                Touch second_Touch = Input.GetTouch(1);
                
                if(first_Touch.phase == TouchPhase.Moved && second_Touch.phase == TouchPhase.Moved)
                { 
                    Pinch_Zoom();
                }
                //Product_Desc_Show.instance.rotator.GetComponent<RotateObject>().enabled = false;
            
            }
            else
            {
                zoomObj = false;
            }
        }
    }

    void Scroll_Zoom()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {

            mainCamera.fieldOfView++;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {

            mainCamera.fieldOfView--;
        }
        mainCamera.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, Product_Desc_Show.instance.rotator.minFov, Product_Desc_Show.instance.rotator.maxFov);
    }
    void Pinch_Zoom()
    {
        Touch touchZero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);

        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

        float difference = (currentMagnitude - prevMagnitude) * 0.01f;
        if (prevMagnitude > currentMagnitude)
        {
            mainCamera.fieldOfView++;
        }
        else if (prevMagnitude < currentMagnitude)
        {
            mainCamera.fieldOfView--;
        }
        mainCamera.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, Product_Desc_Show.instance.rotator.minFov, Product_Desc_Show.instance.rotator.maxFov);
    }
    
}


