using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{

    [SerializeField] float rotation_Speed;


    Camera orthoCam;
    Vector3 worldPosDiff;
    Vector3 startWorldPos = Vector2.zero;
    bool swipeCheck = false;
    RaycastHit hit;
    
    //public Vector3 angles;
    public float Obj_Scale_For_AR;
    public float zPos_Of_Obj;
    public float y_Rotation;
    public float minFov = 0f;
    public float maxFov = 0f;

    //public float minXRotation;
    //public float maxXRotation;

    public static RotateObject instance;
    private void Awake()
    {
        instance = this;
        //DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        orthoCam = UI_Manager.instance.canvasCamera;

    }




    private void OnMouseDrag()
    {
        //Ray ray = orthoCam.ScreenPointToRay(Input.mousePosition);
        Debug.Log("In mouse Drag");
        //Debug.Log(Physics.Raycast(ray, out hit));
        //if(swipeCheck)
        // 
        //isDragging = true;
        //Debug.Log(Input.mousePosition);
        //Ray ray = orthoCam.ScreenPointToRay(Input.mousePosition);

        /*if (hit.collider.name == "Upper_Curve")
        {
            swipeCheck = true;
            startWorldPos = Input.mousePosition;
        }
        else
        {
            Debug.Log("Nothing to hit");
        }*/




    }

    //private void OnMouse
    // Update is called once per frame
    void Update()
    {
        CheckForObject();
        //transform.Rotate(Vector3.forward);    
    }
    private void FixedUpdate()
    {
        //if (transform.localEulerAngles.x >= 180)
        //{
            //angles = transform.eulerAngles;
        //}
        //Debug.Log(transform.rotation.x);
        //angles = transform.rotation
        
        if (swipeCheck && !Zooming_Obj.zoomObj)
        {
            //Debug.Log(Time.fixedDeltaTime);
            Vector3 endWorldPos = orthoCam.ScreenToWorldPoint(Input.mousePosition);
            worldPosDiff = endWorldPos - startWorldPos;
            //Debug.Log(transform.eulerAngles+"  "+transform.localEulerAngles);
            // Debug.Log("Pos Diff " + worldPosDiff);
            worldPosDiff.Normalize();
            startWorldPos = endWorldPos;
            //Debug.Log("normalized diff: " + worldPosDiff);
            if (worldPosDiff.x > 0 && worldPosDiff.y > -0.5f && worldPosDiff.y < 0.5f)
            {
                //swipeCheck = false;

                //transform.localEulerAngles = new Vector3(0f, transform.localEulerAngles.y, transform.localEulerAngles.z);
                transform.Rotate(Vector3.down * rotation_Speed * Time.fixedDeltaTime);


            }
            else if (worldPosDiff.x < 0 && worldPosDiff.y > -0.5f && worldPosDiff.y < 0.5f)
            {
                //Debug.Log("Diff is < 0: " +pixelDiff);
                //swipeCheck = false;
                //transform.localEulerAngles = new Vector3(0f, transform.localEulerAngles.y, transform.localEulerAngles.z);
                transform.Rotate(Vector3.up * rotation_Speed * Time.fixedDeltaTime);
            }
            else if (worldPosDiff.y < 0 && worldPosDiff.x > -0.5f && worldPosDiff.x < 0.5f)
            {


                
               
                    //if(transform.rotation.x)
                    //if(transform.localEulerAngles.x <= maxXRotation && transform.localEulerAngles.x >= minXRotation-360)
                    // {

                    //Debug.Log(trans+_rm.localEulerAngles.x);
                    //Debug.Log("Z-ROtation "+transform.localEulerAngles.z+"   max Rotation:"+maxZRotation);
                    //if (transform.localEulerAngles.x <= maxZRotation && transform.localEulerAngles.x >= minZRotation)
                    //{
                    //Debug.Log("Rotation Condition if");
                    // swipeCheck = false;
                    //transform.localEulerAngles = new Vector3(0f,transform.localEulerAngles.y, transform.localEulerAngles.z);
                    transform.Rotate(Vector3.left * rotation_Speed * Time.fixedDeltaTime);
                //Debug.Log(transform.eulerAngles);
                //Mathf.Clamp(transform.localEulerAngles.x*Time.fixedDeltaTime,minXRotation,maxXRotation);
                //transform.Rotate()
                //float x = Mathf.Clamp(transform.localRotation.x,0,-35f);
                //transform.localRotation = Quaternion.Euler(new Vector3(x,0,0));
                //}

                //}
            }
            else if (worldPosDiff.y > 0 && worldPosDiff.x > -0.5f && worldPosDiff.x < 0.5f)
            {
                //Debug.Log("x-diff: " + worldPosDiff.y + " y diff: " + worldPosDiff.x);
                // if (transform.localEulerAngles.z >= minZRotation && transform.localEulerAngles.z <= maxZRotation + 3)
                //{
                //Debug.Log("Rotation Condition if");
                //swipeCheck = false;
                //transform.localEulerAngles = new Vector3(0f,transform.localEulerAngles.y, transform.localEulerAngles.z);
                transform.Rotate(Vector3.right * rotation_Speed * Time.fixedDeltaTime);

                //}
            }


            //Debug.Log("y position in world: " + worldPos.y);
        }
    }

    void CheckForObject()
    {
        Ray ray = orthoCam.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit))
        {

            if (hit.collider.name == "Upper_Curve")
            {

                startWorldPos = orthoCam.ScreenToWorldPoint(Input.mousePosition);
                swipeCheck = true;

            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            swipeCheck = false;
        }
    }
    public void ResetObject()
    {
        transform.localRotation = Quaternion.identity;
        Camera.main.fieldOfView = 60;
    }
}


//if (Input.GetMouseButtonUp(0))
//{
//    Debug.Log("In Mouse Up");
//    isDragging = false;
//    rb.AddTorque(Vector3.zero);
//}
//if (isDragging)
//{
//    //transform.Rotate(Vector3.up * rotation_Speed * Time.fixedDeltaTime);
//    transform.Rotate(Vector3.down * rotation_Speed * Time.fixedDeltaTime);
//}



//if (isDragging)
//{
//    transform.Rotate(Vector3.up * rotation_Speed * Time.fixedDeltaTime);
//}

//if (isDragging)
//{
//    if(Input.mousePosition.x - startPos.x > 0)
//    { 

//        float x = Input.GetAxis("Mouse X") * rotation_Speed * Time.fixedDeltaTime;
//        Debug.Log(Vector3.up * x);
//        rb.AddTorque(Vector3.up * x);
//    }
//    else if(Input.mousePosition.x - startPos.x < 0)
//    {
//        float x = Input.GetAxis("Mouse X") * rotation_Speed * Time.fixedDeltaTime;
//        Debug.Log(Vector3.down * x);
//        rb.AddTorque(Vector3.down * x);
//    }
//   // float y = Input.GetAxis("Mouse Y") * rotation_Speed * Time.fixedDeltaTime;


//    //Debug.Log("Value of X-axis: " + x);
//    //Debug.Log("Value of y-axis: " + y);

//}


