using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

public class UI_Manager : MonoBehaviour
{
    public Spawned_Obj_Scriptable _Scriptable;
    public Camera canvasCamera;
    public GameObject imageOf3DModel;
    public GameObject splash_Screen;
    public GameObject login_Screen;
    public GameObject product_category_Screen;
    public GameObject product_list_Screen;
    public GameObject product_desc_Screen;
    
    //public GameObject[] screen_List;
    //public GameObject currentScreen;
    //public GameObject previousScreen;

    public InputField userID_inputfield;
    public InputField password_inputfield;


    public GameObject user_Id_Warnmsg;
    public GameObject password_Warnmsg;
    
    public Stack stack = new Stack();
    public Transform place_Obj;
    int child_Count = 0;
   
    public static UI_Manager instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the  update
    void Start()
    {
        Load_Screen();
        stack.Clear();
        child_Count = GameObject.Find("Canvas").transform.childCount;
        
        //PlayerPrefs.SetInt()
        //login_Screen.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.Space)|| Input.GetKeyDown(KeyCode.Escape))
        {

            /*if(Obj_List.Count != 0)
            {
                for(int i = 0; i < Obj_List.Count; i++)
                {
                    Obj_List.IndexOf()
                    //Obj_List.i
                }
            }
            else
            {
                Debug.Log("Do Something...");
            }*/
            if(stack.Count > 0)
            {
                GameObject obj = (GameObject)stack.Pop();
                
                Disable_Obj();
                //obj.transform.SetParent(GameObject.Find("Canvas").transform);
                obj.SetActive(true);
                //GameObject.Find("Canvas").transform.GetChild()
            }
            else if(product_category_Screen.activeInHierarchy && stack.Count == 0)
            {
                ARTapToPlaceObject.instance.obj.objects.Clear();
                Application.Quit();
            }
              
            
        }
    }
    void Disable_Obj()
    {
        //Debug.Log(child_Count);
       
        for(int i = 2; i < child_Count; i++)
        {
            //Debug.Log(GameObject.Find("Canvas").transform.GetChild(i).gameObject.name);
            GameObject.Find("Canvas").transform.GetChild(i).gameObject.SetActive(false);
        }
    }


    void Load_Screen()
    {
        if(PlayerPrefs.GetInt("LoadScreenNo") == 1)
        {
            product_category_Screen.SetActive(true);
            login_Screen.SetActive(false);
            splash_Screen.SetActive(false);
        }
        else
        {
            splash_Screen.SetActive(true);
            Invoke("Login_Scr_Display", 2.5f);
        }
    }
    void Login_Scr_Display()
    {
        Debug.Log("In Login Screen Load");
        login_Screen.SetActive(true);
        splash_Screen.SetActive(false);
    }

    public void Login_Clicked()
    {


        if (userID_inputfield.text == "")
        {
            user_Id_Warnmsg.SetActive(true);
            user_Id_Warnmsg.GetComponentInChildren<Text>().text = "Enter a UserId";
        }



        if (password_inputfield.text == "")
        {
            password_Warnmsg.SetActive(true);
            password_Warnmsg.GetComponentInChildren<Text>().text = "Enter Password";
        }

        if (userID_inputfield.text == "Admin" && password_inputfield.text == "admin")
        {
            product_category_Screen.SetActive(true);
            login_Screen.SetActive(false);
        }

    }

    public void User_Id_ValueChanged(string id)
    {


        if (id != "Admin")
        {
            user_Id_Warnmsg.SetActive(true);
            user_Id_Warnmsg.GetComponentInChildren<Text>().text = "Enter valid UserId";
        }
        else if (id == "Admin")
        {
            user_Id_Warnmsg.SetActive(false);

        }
    }

    public void Password_ValueChanged(string passwrd)
    {
        if (passwrd != "admin")
        {
            password_Warnmsg.SetActive(true);
            password_Warnmsg.GetComponentInChildren<Text>().text = "Enter valid Password";
        }
        else if (passwrd == "admin")
        {
            password_Warnmsg.SetActive(false);

        }
    }


    public void AR_Btn_Clicked()
    {
        
        
        Add_Current_Screen(product_desc_Screen);
        SceneManager.LoadScene(1);
        
        
    }

    //public void Close_Btn_Clicked()
    //{
    //    popup_msgbox.SetActive(false);
    //}


    //public void Bathroom_Accessories_Btn_Clicked()
    //{
    //    product_list_Screen.SetActive(true);
    //}

    //public void Tiles_Btn_Clicked()
    //{
    //    product_list_Screen.SetActive(true);
    //}

    public void Back_Btn_Clicked()
    {

        //Find_Current_Screen();
        //previousScreen =
        //product_category_Screen.SetActive(true);
        // product_list_Screen.SetActive(false);
    }


    public void Menu_Btn(string name)
    {
        Debug.Log(name);
        //Add_Current_Screen();

    }
    public void Add_Current_Screen(GameObject obj)
    {
        //Obj_List.Add(obj);
        //Debug.Log(obj.name);
       // obj.transform.SetParent(place_Obj);
        
        stack.Push(obj);

       
        /*for(int i = 0; i < screen_List.Length; i++)
        {
            if(screen_List[i].activeInHierarchy)
            {
                currentScreen  = screen_List[i];
                //previousScreen = screen_List[i]-1;
            }
        }*/
    }
    public void Search_Btn_Clicked()
    {

    }





    //public void product_Btn_Clicked(string product_name)
    //{
    //    product_list_Screen.SetActive(false);
    //    product_desc_Screen.SetActive(true);
    //    Product_Desc_Display(product_name);


    //}


}
