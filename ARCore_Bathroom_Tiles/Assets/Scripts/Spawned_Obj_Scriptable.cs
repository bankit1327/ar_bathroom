using System.Collections;
using System.Collections.Generic;
using UnityEngine;

  [CreateAssetMenu(fileName = "new Object", menuName = "Objects")]

    public class Spawned_Obj_Scriptable : ScriptableObject
    {
        public List<InstObject> objects; //= new List<InstObject>();
    }

    [System.Serializable]
    public struct InstObject
    {
        public GameObject objToSpwan;
        public Vector3 position;
        public Quaternion rotation;
    }

