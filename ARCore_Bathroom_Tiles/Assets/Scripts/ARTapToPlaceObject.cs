using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ARTapToPlaceObject : MonoBehaviour
{
    public GameObject gameObjectToInst;
    private GameObject spawnedObj;
    private ARRaycastManager aRRaycastManager;
    private Vector2 touchPos;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    public GameObject[] objects;
    public Text loaded_All;
    //public Text playerprefs_Val;
    
    
    public Spawned_Obj_Scriptable obj;
    public InstObject obj_Struct;
    
    public static ARTapToPlaceObject instance;
    private void Awake()
    {
       aRRaycastManager = GetComponent<ARRaycastManager>();
    }


    bool GetTouchPosition(out Vector2 touchPos)
    {
        if(Input.touchCount > 0)
        {
            touchPos = Input.GetTouch(0).position;
            return true;
        }
        touchPos = default;
        return false;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        /*int total_Obj = obj.objects.Count;
        Debug.Log(total_Obj);
        if (total_Obj > 0)
        {
            foreach (InstObject obj in obj.objects)
            {
                GameObject inst_Obj = Instantiate(obj.objToSpwan, obj.position, obj.rotation);
                Debug.Log(inst_Obj.name);
            }
        }
        else
        {
            Debug.Log("Dont have objects to diplay for the 1st time");
        }*/
        //Back_Clicked();
        //PlayerPrefs.SetString("Product_Name","Faucet");
        Load_Assign_Obj_To_HelloAR();
    }

    public void Load_Assign_Obj_To_HelloAR()
    {
        //Debug.Log(PlayerPrefs.GetString("Product_Name"));
        loaded_All.text = loaded_All.text+ "  " +PlayerPrefs.GetString("Product_Name");
        GameObject obj = Array.Find(objects, o => o.name == PlayerPrefs.GetString("Product_Name"));
        
        //Debug.Log(PlayerPrefs.GetString("Product_Name"));
        //assign_to_AR.
        if (obj != null)
        {
            gameObjectToInst = obj;
            loaded_All.text = loaded_All.text+ "  " + gameObjectToInst.name;
            //_Obj.objects.Add(obj);
        }
    }

    public void Back_Clicked()
    {
        //spawnedObj = gameObjectToInst;
        if (spawnedObj != null)
        {
            struct_Obj_Initialize();
            Add_Structure_To_List();
        }
        SceneManager.LoadScene(0);
        PlayerPrefs.SetInt("LoadScreenNo", 1);

    }

    public void struct_Obj_Initialize()
    {
        
            obj_Struct.objToSpwan = spawnedObj;
            obj_Struct.position = spawnedObj.transform.position;
        
        //spawnedObj = gameObjectToInst;
        
    }
    public void Add_Structure_To_List()
    {
        
        obj.objects.Add(obj_Struct);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //SceneManager.LoadScene(0);
            Back_Clicked();
        }
        if (!GetTouchPosition(out Vector2 touchPos))
            return;
        if(aRRaycastManager.Raycast(touchPos,hits,TrackableType.PlaneWithinPolygon))
        {
            var hitPose = hits[0].pose;
            if(spawnedObj == null)
            {
                spawnedObj = Instantiate(gameObjectToInst,hitPose.position,hitPose.rotation);
            }
            else
            {
                spawnedObj.transform.position = hitPose.position;
                
            }
        }

        
    }
}
