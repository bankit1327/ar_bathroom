using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchHandler : MonoBehaviour
{
    [SerializeField] float rotation_Speed;
    Touch touch;
    Quaternion rotationY;
    float speedModifier = 0.1f;
    bool fingerDown;
    //Vector2 startPos;
    Vector2 endpos;
    Vector2 possDiff;
    public int pixelTodetect = 50;
    public Text checkText;
    
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;



    // If the touch is longer than MAX_SWIPE_TIME, we dont consider it a swipe
    public const float MAX_SWIPE_TIME = 0.5f;

    // Factor of the screen width that we consider a swipe
    // 0.17 works well for portrait mode 16:9 phone
    public const float MIN_SWIPE_DISTANCE = 0.17f;

    public static bool swipedRight = false;
    public static bool swipedLeft = false;
    public static bool swipedUp = false;
    public static bool swipedDown = false;


    public bool debugWithArrowKeys = true;

    Vector2 startPos;
    float startTime;

    void Start()
    {
        fingerDown = false;
    }

    //private void OnMouseDrag()
    //{
        
    //    fingerDown = true;
    //}
    

    // Update is called once per frame
    void Update()
    {
        swipedRight = false;
        swipedLeft = false;
        swipedUp = false;
        swipedDown = false;

        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                startPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);
                startTime = Time.time;
            }
            if (t.phase == TouchPhase.Ended)
            {
                if (Time.time - startTime > MAX_SWIPE_TIME) // press too long
                    return;

                Vector2 endPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);

                Vector2 swipe = new Vector2(endPos.x - startPos.x, endPos.y - startPos.y);

                if (swipe.magnitude < MIN_SWIPE_DISTANCE) // Too short swipe
                    return;

                if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
                { // Horizontal swipe
                    if (swipe.x > 0)
                    {
                        swipedRight = true;
                        transform.Rotate(Vector3.down * rotation_Speed * Time.deltaTime);
                    }
                    else
                    {
                        swipedLeft = true;
                        transform.Rotate(Vector3.up * rotation_Speed * Time.deltaTime);
                    }
                }
                else
                { // Vertical swipe
                    if (swipe.y > 0)
                    {
                        swipedUp = true;
                        transform.Rotate(Vector3.back * 50f * Time.deltaTime);
                    }
                    else
                    {
                        swipedDown = true;
                        transform.Rotate(Vector3.forward * 150f * Time.deltaTime);
                    }
                }
            }
        }

        if (debugWithArrowKeys)
        {
            swipedDown = swipedDown || Input.GetKeyDown(KeyCode.DownArrow);
            swipedUp = swipedUp || Input.GetKeyDown(KeyCode.UpArrow);
            swipedRight = swipedRight || Input.GetKeyDown(KeyCode.RightArrow);
            swipedLeft = swipedLeft || Input.GetKeyDown(KeyCode.LeftArrow);
        }
    }

    //transform.Rotate(Vector3.back);
    /*
    if (Input.GetMouseButtonDown(0))
     {
     firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
     //save began touch 2d point

     }
     if (fingerDown)
     {
         //save ended touch 2d point
         secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

         //create vector from the two points
         currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
         Debug.Log("Current Swipe: "+currentSwipe);
         //normalize the 2d vector
         currentSwipe.Normalize();
         Debug.Log("Normalize Current Swipe: "+currentSwipe);
         //swipe upwards
         if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
         {
             fingerDown = false;
             //Debug.Log("up swipe");
             transform.Rotate(Vector3.back * rotation_Speed * Time.deltaTime);
         }   
         //swipe down
         if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
         {
             fingerDown = false;
             //Debug.Log("down swipe");
             transform.Rotate(Vector3.forward * rotation_Speed * Time.deltaTime);
         }
         //swipe left
         if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
         {
             fingerDown = false;
             //Debug.Log("left swipe");
             transform.Rotate(Vector3.up * rotation_Speed * Time.deltaTime);
         }
         //swipe right
         if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
         {
             fingerDown = false;
             transform.Rotate(Vector3.down * rotation_Speed * Time.deltaTime);
             //Debug.Log("right swipe");
         }
     }*/


    /*if(Input.touchCount > 0 && fingerDown == false && Input.touches[0].phase == TouchPhase.Began)
    {
        fingerDown = true;
        startPos = Input.touches[0].position;
        checkText.text = "Start pos: "+startPos.ToString();
        //touch = Input.GetTouch(0);

        //if(touch.phase == TouchPhase.Moved)
        //{
        //    rotationY = Quaternion.Euler(0f,-touch.deltaPosition.x* speedModifier,0f);
        //    transform.rotation = rotationY * transform.rotation;
        //}
    }
    if(fingerDown)
    {
        endpos = Input.touches[0].position;
        currentSwipe = endpos - startPos;
        currentSwipe.Normalize();
        checkText.text = "End pos: "+currentSwipe.ToString();
        if(currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            fingerDown = false;
            checkText.text = "Swipe right";
            transform.Rotate(Vector3.down * rotation_Speed * Time.deltaTime);
        }
        else if(currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            fingerDown = false;
            checkText.text = "Swipe left";
            transform.Rotate(Vector3.up * rotation_Speed * Time.deltaTime);
        }
        else if(currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            fingerDown = false;
            checkText.text = "Swipe Up";
            transform.Rotate(Vector3.back * 25f * Time.deltaTime);
        }
        else if(currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            fingerDown = false;
            checkText.text = "Swipe Down";
            transform.Rotate(Vector3.forward * 50f * Time.deltaTime);
        }
    }*/






}

