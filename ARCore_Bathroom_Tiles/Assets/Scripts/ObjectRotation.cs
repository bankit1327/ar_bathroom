using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{

    private bool rotateObject = false;
    public float rotationSpeed = 280f;
    public float minFov = 0f;
    public float maxFov = 0f;
    
    Vector3 prevMousePos = Vector2.zero;
    Camera cam;
    void Start()
    {
        cam = UI_Manager.instance.canvasCamera;
    }
    void Update()
    {
        CheckAndStartTORotate();
        //angles = transform.localEulerAngles;
    }

    void FixedUpdate()
    {
        if (rotateObject)
            RotateObject();
    }

    private void RotateObject()
    {
        Vector3 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        //Debug.Log("End Pos: "+mousePos);
        Vector2 difference  = Vector3.Normalize(mousePos - prevMousePos);
        Debug.Log("Mouse Pos: "+(mousePos - prevMousePos)+"  Normalized Mouse Pos: "+difference);
        float speed = Time.fixedDeltaTime * rotationSpeed;
        prevMousePos = mousePos;
        //Debug.Log(Time.fixedDeltaTime);
        // Debug.Log(speed);
        //Debug.Log(-difference.x * speed);
        
        transform.Rotate(Vector3.up, -difference.x * speed, Space.World);
            
        
        transform.Rotate(Vector3.right, difference.y * speed, Space.World);
        


        /*float xPos = Mathf.Clamp(transform.localRotation.x,-20f,20f);
        transform.Rotate(new Vector3(xPos,0,0));
        prevMousePos = mousePos;*/
    }

    private void CheckAndStartTORotate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray _ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit _hit;
            if (Physics.Raycast(_ray, out _hit))
            {
                if (_hit.collider.transform.name == UI_Manager.instance.imageOf3DModel.name)
                {
                    rotateObject = true;
                    prevMousePos = cam.ScreenToWorldPoint(Input.mousePosition);
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            rotateObject = false;
        }
    }

    public void ResetObject()
    {
        transform.localRotation = Quaternion.identity;
        Camera.main.fieldOfView = 60;
    }
}
